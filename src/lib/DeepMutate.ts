import { Primitive, UnionKeyOf } from "ts-typetools";

import { DeepMutatePath } from "./DeepMutatePath";
import { DeepPick } from "./DeepPick";
import { DeepPickGrammar, DefaultGrammar } from "./DeepPickGrammar";
import { DeepPickPath } from "./DeepPickPath";
import { Opaque } from "./Opaque";

export type DeepMutate<
  T,
  K extends DeepMutatePath<T, G>,
  M extends MutationSpec<DeepPick<T, Extract<K, DeepPickPath<T>>>, G>,
  G extends DeepPickGrammar = DefaultGrammar
> = T extends Opaque
  ? T
  : [K] extends [G["glob"]]
  ? Mutate<T, M, G>
  : T extends Primitive
  ? T
  : T extends Array<infer Item>
  ? [K] extends [`${G["array"]}${G["prop"]}${infer K}`]
    ? DeepMutate<Item, K, InnerMutationSpec<G["array"], M, G>, G>[]
    : [K] extends [`${G["array"]}`]
    ? DeepMutate<Item, G["glob"], InnerMutationSpec<G["array"], M, G>, G>[]
    : never
  : Rename<
      {
        [key in UnionKeyOf<T>]: [K] extends [`${key}${G["prop"]}${infer K}`]
          ? DeepMutate<T[key], K, InnerMutationSpec<key, M, G>, G>
          : [K] extends [key]
          ? DeepMutate<T[key], G["glob"], InnerMutationSpec<key, M, G>, G>
          : T[key];
      },
      T
    >;

type Rename<T, Alias extends T> = T extends Alias
  ? Alias extends T
    ? Alias
    : T
  : T;

type InnerKey<key extends string, K, G extends DeepPickGrammar> = [
  K extends `${key}${G["prop"]}${infer K}` ? K : never
][0];

type Mutate<
  T,
  Spec extends MutationSpec<T, G>,
  G extends DeepPickGrammar
> = Spec extends string
  ? DeepPick<T, Extract<Spec, DeepPickPath<T, G>>, G>
  : Spec extends { extract: infer U }
  ? Extract<T, U>
  : Spec extends { exclude: infer U }
  ? Exclude<T, U>
  : Spec extends { replace: infer U }
  ? U
  : T;

type InnerMutationSpec<
  key extends string,
  M extends MutationSpec<any, G>,
  G extends DeepPickGrammar
> = M extends string ? InnerKey<key, M, G> : M;

export type MutationSpec<T, G extends DeepPickGrammar> =
  | DeepPickPath<T, G>
  | { extract: any }
  | { exclude: any }
  | { replace: any };
