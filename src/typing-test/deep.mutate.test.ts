import {
  ExactType,
  OmitStrict,
  SubType,
  Unrelated,
  expectType,
} from "ts-typetools/lib";

import { DeepMutate, DeepMutatePath } from "../lib";
import { Opaque } from "../lib/Opaque";

interface ABC<T> {
  a: T;
  b: T;
  c: T;
}

// objects
{
  expectType<
    DeepMutate<ABC<{ foo: number | string }>, "a.foo", { exclude: string }>
  >()
    .assert<
      OmitStrict<ABC<{ foo: number | string }>, "a"> & { a: { foo: number } }
    >()
    .toBe(ExactType);

  expectType<
    DeepMutate<ABC<{ foo: number | string }>, "a.foo", { replace: boolean }>
  >()
    .assert<
      OmitStrict<ABC<{ foo: number | string }>, "a"> & { a: { foo: boolean } }
    >()
    .toBe(ExactType);

  expectType<
    DeepMutate<ABC<{ foo: 2 | string }>, "a.foo", { extract: number }>
  >()
    .assert<OmitStrict<ABC<{ foo: 2 | string }>, "a"> & { a: { foo: 2 } }>()
    .toBe(ExactType);

  expectType<DeepMutate<ABC<{ foo: 2 | string }>, "*", { replace: number }>>()
    .assert<number>()
    .toBe(ExactType);
}

// with arrays
{
  expectType<
    DeepMutate<ABC<{ foo: 2 | string }[]>, "a.[].foo", { extract: number }>
  >()
    .assert<OmitStrict<ABC<{ foo: 2 | string }[]>, "a"> & { a: { foo: 2 }[] }>()
    .toBe(ExactType);

  expectType<
    DeepMutate<ABC<{ foo: number | string }[]>, "a.[].foo", { exclude: string }>
  >()
    .assert<
      OmitStrict<ABC<{ foo: number | string }[]>, "a"> & {
        a: { foo: number }[];
      }
    >()
    .toBe(ExactType);

  expectType<
    DeepMutate<
      ABC<{ foo: number | string }[]>,
      "a.[].foo",
      { replace: boolean }
    >
  >()
    .assert<
      OmitStrict<ABC<{ foo: number | string }[]>, "a"> & {
        a: { foo: boolean }[];
      }
    >()
    .toBe(ExactType);

  expectType<
    DeepMutate<ABC<{ foo: number | string }[]>, "*", { replace: number }>
  >()
    .assert<number>()
    .toBe(ExactType);

  expectType<
    DeepMutate<ABC<{ foo: number | string }[]>, "a.[]", { replace: number }>
  >()
    .assert<
      OmitStrict<ABC<{ foo: number | string }[]>, "a"> & {
        a: number[];
      }
    >()
    .toBe(ExactType);
}

// opaque modifiers
{
  expectType<
    DeepMutatePath<{ a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } }>
  >()
    .assert<"d.e" | "d.f" | "d.f.g">()
    .toBe(SubType);

  expectType<
    DeepMutatePath<{
      a: { b: 0; c: 0 };
      d: { e: 0; f: { g: 0; h: 0 } } & Opaque;
    }>
  >()
    .assert<"d.e" | "d.f" | "d.f.g">()
    .toBe(Unrelated);

  expectType<
    DeepMutatePath<
      { a: { b: 0; c: 0 }; d: { e: 0; f: { g: 0; h: 0 } } } & Opaque
    >
  >()
    .assert<"*">()
    .toBe(ExactType);
}
